import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  orders: any;
  ordID: any;
  ordStatus: any;
  custInput = {
    orderID: '',
    thingTwo: ''
  };

  constructor(public navCtrl: NavController, public http: Http) {
  }


  whereForm(form) {
    var url = 'http://localhost:8080/wheresmyorder-ws/rest/where/' + encodeURI(this.custInput.orderID);
    console.log('calling: ' + url)
    this.http.get(url).map(res => res.json()).subscribe(data => {
        this.orders = data;
        this.ordID = data.id;
        this.ordStatus = data.status;
        console.log(data);
        return data;
    },
    err => {
        console.log('Oops!');
    },
    () => console.log('order search complete')
    );
	
  }

}
